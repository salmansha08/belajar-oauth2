const passport = require("passport");
const OAuth2Strategy = require("passport-oauth2").Strategy;

require("dotenv").config();

const PORT = process.env.PORT;
const HOST = process.env.HOST;
const SSL = process.env.SSL;

//IDCH
passport.use(
  new OAuth2Strategy(
    {
      authorizationURL: "https://my.idcloudhost.com/oauth/authorize.php",
      tokenURL: "https://my.idcloudhost.com/oauth/token.php",
      clientID: process.env.IDCH_CLIENT_ID,
      clientSecret: process.env.IDCH_CLIENT_SECRET,
      callbackURL: `${SSL}://${HOST}:${PORT}/auth/idcloudhost/callback`,
    },
    function (request, accessToken, refreshToken, profile, done) {
      done(null, profile);
    }
    /*
  function(accessToken, refreshToken, profile, cb) {
    User.findOrCreate({ exampleId: profile.id }, function (err, user) {
      return cb(err, user);
    });
  }
  */
  )
);

console.log("ID " + process.env.IDCH_CLIENT_ID);
console.log("SECRET " + process.env.CLIENT_SECRET);

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});
