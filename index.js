const express = require("express");
const session = require("express-session");
const passport = require("passport");
const fs = require("fs");
const https = require("https");
const path = require("path");

const app = express();
require("dotenv").config();

require("./auth");

app.use(express.json());
app.use(express.static(path.join(__dirname, "client")));

const PORT = process.env.PORT;
const HOST = process.env.HOST;
const SSL = process.env.SSL;

// MAIN PAGE
app.get("/", (req, res) => {
  res.sendFile("index.html");
});

app.use(
  session({
    secret: process.env.mysecretkey,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }, // False if  http
  })
);

app.use(passport.initialize());
app.use(passport.session());

function generateRandomState() {
  return Math.random().toString(36).substring(7);
}

function isLoggedIn(req, res, next) {
  res.user ?  next() : res.sendStatus(401);
}

//IDCH
app.get("/auth/idcloudhost", (req, res, next) => {
  const state = generateRandomState(); // Generate a random state value
  req.session.oauth2state = state; // Store the state in the session
  passport.authenticate("oauth2", {
    state: state, // Provide the state as a parameter
  })(req, res, next);
});

app.get(
  "/auth/idcloudhost/callback",
  passport.authenticate("oauth2", {
    successRedirect: "/auth/idcloudhost/success",
    failureRedirect: "/auth/idcloudhost/failure",
  })
);

app.get("/auth/idcloudhost/success", isLoggedIn, (req, res) => {
  let name = req.user.displayName;
  res.send(`Hello ${name}`);
});

app.get("/auth/idcloudhost/failure", (req, res) => {
  res.send("Gagal Login IDCloudHost!");
});

app.use("/auth/idcloudhost/logout", (req, res) => {
  req.session.destroy();
  res.send("Berhasil Logout IDCloudHost!");
});

const sslServer = https.createServer(
  {
  key: fs.readFileSync(__dirname + process.env.SSL_KEY),
  cert: fs.readFileSync(__dirname + process.env.SSL_CERT),
  },
  app
)

sslServer.listen(5000, () => {
  console.log(`Listening ... Open ${SSL}://${HOST}:${PORT}`);
});
